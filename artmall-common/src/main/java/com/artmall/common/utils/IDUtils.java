package com.artmall.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @Description: ID 生成策略
 *
 * @author MING
 * @eamil lmm_work@163.com
 * @date 2018/11/20 10:27
 */
public class IDUtils {

	/**
	 * 图片名生成
	 */
	public static String genImageName() {
		//取当前时间的长整形值包含毫秒
		long millis = System.currentTimeMillis();
		//long millis = System.nanoTime();
		//加上三位随机数
		Random random = new Random();
		int end3 = random.nextInt(999);
		//如果不足三位前面补0
		String str = millis + String.format("%03d", end3);
		
		return str;
	}
	
	/**
	 * 商品id生成
	 */
	public static long genItemId() {
		//取当前时间的长整形值包含毫秒
		long millis = System.currentTimeMillis();
		//long millis = System.nanoTime();
		//加上两位随机数
		Random random = new Random();
		int end2 = random.nextInt(99);
		//如果不足两位前面补0
		String str = millis + String.format("%02d", end2);
		long id = new Long(str);
		return id;
	}

	/**
	 * 商品id生成 2 String
	 */
	public static String getId() {
		//取当前时间的长整形值包含毫秒
		long millis = System.currentTimeMillis();
		//long millis = System.nanoTime();
		//加上两位随机数
		Random random = new Random();
		int end2 = random.nextInt(99);
		//如果不足两位前面补0
		String str = millis + String.format("%02d", end2);
		return str;
	}

	// 利用时间戳得到8位不重复的随机数
	public static String getsid(){
		long nowDate = System.currentTimeMillis();
		String sid = Integer.toHexString((int)nowDate);
		System.out.println(sid);
		return sid;
	}

	// 采集编号
	public static String getcid(){
		long nowDate = System.currentTimeMillis();
//		String sid = Integer.toHexString((int)nowDate);
		String cid = "cj" + nowDate;
		System.out.println(cid);
		return cid;
	}

	public static void main(String[] args) {
//		for(int i=0;i< 100;i++){
			System.out.println(getcid());
//		}

	}
}
