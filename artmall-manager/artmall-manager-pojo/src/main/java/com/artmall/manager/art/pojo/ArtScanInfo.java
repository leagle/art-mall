package com.artmall.manager.art.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

public class ArtScanInfo implements Serializable {

    @TableId(value = "scan_id",type = IdType.INPUT)
    private String scanId;

    private String hyperspectral;

    private String highPrecision;

    private String trueColor;

    private Date scanTime;

    private String scanReason;

    private String artId;

    private String icdn;

    private static final long serialVersionUID = 1L;

    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId == null ? null : scanId.trim();
    }

    public String getHyperspectral() {
        return hyperspectral;
    }

    public void setHyperspectral(String hyperspectral) {
        this.hyperspectral = hyperspectral == null ? null : hyperspectral.trim();
    }

    public String getHighPrecision() {
        return highPrecision;
    }

    public void setHighPrecision(String highPrecision) {
        this.highPrecision = highPrecision == null ? null : highPrecision.trim();
    }

    public String getTrueColor() {
        return trueColor;
    }

    public void setTrueColor(String trueColor) {
        this.trueColor = trueColor == null ? null : trueColor.trim();
    }

    public Date getScanTime() {
        return scanTime;
    }

    public void setScanTime(Date scanTime) {
        this.scanTime = scanTime;
    }

    public String getScanReason() {
        return scanReason;
    }

    public void setScanReason(String scanReason) {
        this.scanReason = scanReason;
    }

    public String getArtId() {
        return artId;
    }

    public void setArtId(String artId) {
        this.artId = artId;
    }

    public String getIcdn() {
        return icdn;
    }

    public void setIcdn(String icdn) {
        this.icdn = icdn;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", scanId=").append(scanId);
        sb.append(", hyperspectral=").append(hyperspectral);
        sb.append(", highPrecision=").append(highPrecision);
        sb.append(", trueColor=").append(trueColor);
        sb.append(", scanTime=").append(scanTime);
        sb.append(", scanReason=").append(scanReason);
        sb.append(", artId=").append(artId);
        sb.append(", icdn=").append(icdn);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}