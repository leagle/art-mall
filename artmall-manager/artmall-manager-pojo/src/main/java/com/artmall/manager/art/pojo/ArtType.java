package com.artmall.manager.art.pojo;

import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;

public class ArtType implements Serializable {

    @TableId
    private Integer artTypeId;

    private String artTypeName;

    private static final long serialVersionUID = 1L;

    public Integer getArtTypeId() {
        return artTypeId;
    }

    public void setArtTypeId(Integer artTypeId) {
        this.artTypeId = artTypeId;
    }

    public String getArtTypeName() {
        return artTypeName;
    }

    public void setArtTypeName(String artTypeName) {
        this.artTypeName = artTypeName == null ? null : artTypeName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", artTypeId=").append(artTypeId);
        sb.append(", artTypeName=").append(artTypeName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}