package com.artmall.manager.art.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class ArtInfo implements Serializable {

    @TableId(value = "art_id",type = IdType.INPUT)
    private String artId;

    private String artName;

    private String artCount;

    private Integer artStatus;

    private Integer status;

    private String artMaterial;

    private String artNormsLong;

    private String artNormsWide;

    private String artNormsHeight;

    private String authorId;

    private String years;

    private String icdn;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    private String belongUserId;

    private String belongOrganize;

    private Integer artTypeId;

    private Integer visible;

    private Integer identStatus;

    private Integer iuse;

    private String artDescribe;

    private static final long serialVersionUID = 1L;

    public String getArtId() {
        return artId;
    }

    public void setArtId(String artId) {
        this.artId = artId == null ? null : artId.trim();
    }

    public String getArtName() {
        return artName;
    }

    public void setArtName(String artName) {
        this.artName = artName == null ? null : artName.trim();
    }

    public String getArtCount() {
        return artCount;
    }

    public void setArtCount(String artCount) {
        this.artCount = artCount == null ? null : artCount.trim();
    }

    public Integer getArtStatus() {
        return artStatus;
    }

    public void setArtStatus(Integer artStatus) {
        this.artStatus = artStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getArtMaterial() {
        return artMaterial;
    }

    public void setArtMaterial(String artMaterial) {
        this.artMaterial = artMaterial;
    }

    public String getArtNormsLong() {
        return artNormsLong;
    }

    public void setArtNormsLong(String artNormsLong) {
        this.artNormsLong = artNormsLong == null ? null : artNormsLong.trim();
    }

    public String getArtNormsWide() {
        return artNormsWide;
    }

    public void setArtNormsWide(String artNormsWide) {
        this.artNormsWide = artNormsWide == null ? null : artNormsWide.trim();
    }

    public String getArtNormsHeight() {
        return artNormsHeight;
    }

    public void setArtNormsHeight(String artNormsHeight) {
        this.artNormsHeight = artNormsHeight == null ? null : artNormsHeight.trim();
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId == null ? null : authorId.trim();
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years == null ? null : years.trim();
    }

    public String getIcdn() {
        return icdn;
    }

    public void setIcdn(String icdn) {
        this.icdn = icdn == null ? null : icdn.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getBelongUserId() {
        return belongUserId;
    }

    public void setBelongUserId(String belongUserId) {
        this.belongUserId = belongUserId == null ? null : belongUserId.trim();
    }

    public String getBelongOrganize() {
        return belongOrganize;
    }

    public void setBelongOrganize(String belongOrganize) {
        this.belongOrganize = belongOrganize == null ? null : belongOrganize.trim();
    }

    public Integer getArtTypeId() {
        return artTypeId;
    }

    public void setArtTypeId(Integer artTypeId) {
        this.artTypeId = artTypeId;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getIdentStatus() {
        return identStatus;
    }

    public void setIdentStatus(Integer identStatus) {
        this.identStatus = identStatus;
    }

    public Integer getIuse() {
        return iuse;
    }

    public void setIuse(Integer iuse) {
        this.iuse = iuse;
    }

    public String getArtDescribe() {
        return artDescribe;
    }

    public void setArtDescribe(String artDescribe) {
        this.artDescribe = artDescribe == null ? null : artDescribe.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", artId=").append(artId);
        sb.append(", artName=").append(artName);
        sb.append(", artCount=").append(artCount);
        sb.append(", artStatus=").append(artStatus);
        sb.append(", status=").append(status);
        sb.append(", artMaterial=").append(artMaterial);
        sb.append(", artNormsLong=").append(artNormsLong);
        sb.append(", artNormsWide=").append(artNormsWide);
        sb.append(", artNormsHeight=").append(artNormsHeight);
        sb.append(", authorId=").append(authorId);
        sb.append(", years=").append(years);
        sb.append(", icdn=").append(icdn);
        sb.append(", addTime=").append(addTime);
        sb.append(", belongUserId=").append(belongUserId);
        sb.append(", belongOrganize=").append(belongOrganize);
        sb.append(", artTypeId=").append(artTypeId);
        sb.append(", visible=").append(visible);
        sb.append(", identStatus=").append(identStatus);
        sb.append(", iuse=").append(iuse);
        sb.append(", artDescribe=").append(artDescribe);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}