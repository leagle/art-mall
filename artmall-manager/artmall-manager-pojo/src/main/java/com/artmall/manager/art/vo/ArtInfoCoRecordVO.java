package com.artmall.manager.art.vo;

import com.artmall.manager.art.pojo.ArtCollRecord;
import com.artmall.manager.art.pojo.ArtInfo;
import com.artmall.manager.art.pojo.ArtScanInfo;
import com.artmall.manager.art.pojo.ArtType;

import java.io.Serializable;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/24 17:41
 */
public class ArtInfoCoRecordVO implements Serializable {


    private static final long serialVersionUID = -2838672583077687526L;

    /**
     *  艺术品信息
     */
    private ArtInfo artInfo;

    /**
     * 艺术品类别
     */
    private ArtType artType;

    /**
     *  艺术品采集记录
     */
    private ArtCollRecord artCollRecord;

    /**
     *  艺术品扫描信息
     */
    private ArtScanInfo artScanInfo;

    public ArtInfo getArtInfo() {
        return artInfo;
    }

    public void setArtInfo(ArtInfo artInfo) {
        this.artInfo = artInfo;
    }

    public ArtType getArtType() {
        return artType;
    }

    public void setArtType(ArtType artType) {
        this.artType = artType;
    }

    public ArtCollRecord getArtCollRecord() {
        return artCollRecord;
    }

    public void setArtCollRecord(ArtCollRecord artCollRecord) {
        this.artCollRecord = artCollRecord;
    }

    public ArtScanInfo getArtScanInfo() {
        return artScanInfo;
    }

    public void setArtScanInfo(ArtScanInfo artScanInfo) {
        this.artScanInfo = artScanInfo;
    }
}
