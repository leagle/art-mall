package com.artmall.manager.art.vo;

import com.artmall.manager.art.pojo.*;

import java.io.Serializable;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/23 10:10
 */
public class ArtInfoVO implements Serializable {


    private static final long serialVersionUID = -9838959811295676L;

    /**
     * 艺术品信息
     */
    private ArtInfo artInfo;

    /**
     * 艺术品类别
     */
    private ArtType artType;

    /**
     * 艺术品图片
     */
    private ArtImage artImage;

    /**
     * 艺术品 作者
     */
    private AuthorInfo authorInfo;

    /**
     * 用户信息
     */
    private UserInfo userInfo;


    public ArtInfo getArtInfo() {
        return artInfo;
    }

    public void setArtInfo(ArtInfo artInfo) {
        this.artInfo = artInfo;
    }

    public ArtType getArtType() {
        return artType;
    }

    public void setArtType(ArtType artType) {
        this.artType = artType;
    }

    public ArtImage getArtImage() {
        return artImage;
    }

    public void setArtImage(ArtImage artImage) {
        this.artImage = artImage;
    }

    public AuthorInfo getAuthorInfo() {
        return authorInfo;
    }

    public void setAuthorInfo(AuthorInfo authorInfo) {
        this.authorInfo = authorInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
