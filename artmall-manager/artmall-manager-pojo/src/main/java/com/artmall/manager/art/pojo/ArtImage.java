package com.artmall.manager.art.pojo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class ArtImage implements Serializable {

    @TableId
    private Integer id;

    private String artId;

    private String icdn;

    private String coverImageUrl;

    private String imageUrl;

    private String qrcodeUrl;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date collectTime;

    private Integer status;

    private Integer collectFirst;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArtId() {
        return artId;
    }

    public void setArtId(String artId) {
        this.artId = artId == null ? null : artId.trim();
    }

    public String getIcdn() {
        return icdn;
    }

    public void setIcdn(String icdn) {
        this.icdn = icdn == null ? null : icdn.trim();
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl == null ? null : coverImageUrl.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl == null ? null : qrcodeUrl.trim();
    }

    public Date getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(Date collectTime) {
        this.collectTime = collectTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCollectFirst() {
        return collectFirst;
    }

    public void setCollectFirst(Integer collectFirst) {
        this.collectFirst = collectFirst;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", artId=").append(artId);
        sb.append(", icdn=").append(icdn);
        sb.append(", coverImageUrl=").append(coverImageUrl);
        sb.append(", imageUrl=").append(imageUrl);
        sb.append(", qrcodeUrl=").append(qrcodeUrl);
        sb.append(", collectTime=").append(collectTime);
        sb.append(", status=").append(status);
        sb.append(", collectFirst=").append(collectFirst);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}