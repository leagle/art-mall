package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.AuthorInfo;
import com.baomidou.mybatisplus.service.IService;

public interface AuthorInfoService extends IService<AuthorInfo> {
}
