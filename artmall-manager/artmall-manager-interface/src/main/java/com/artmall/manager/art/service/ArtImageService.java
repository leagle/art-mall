package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtImage;
import com.artmall.manager.art.pojo.ArtInfo;
import com.baomidou.mybatisplus.service.IService;

public interface ArtImageService extends IService<ArtImage> {
    
}
