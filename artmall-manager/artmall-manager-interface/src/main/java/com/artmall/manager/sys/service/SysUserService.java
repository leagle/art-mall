package com.artmall.manager.sys.service;

import com.artmall.common.utils.R;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

public interface SysUserService extends IService<SysUser> {

    /**
     *  根据id 查询用户信息
     */
    SysUser getUserById(Long id);

    /**
     *  根据条件 用户名 、手机号、邮箱 模糊查询
     */
    R getAllUser(Integer pageNo,Integer pageSize,String searchKey,String searchValue);

    /**
     *  添加用户
     */
    void save(SysUser sysUser);

    /**
     * 修改用户
     */
    void update(SysUser sysUser);

    /**
     *  查找用户信息
     */
    SysUser selectOne(SysUser sysUser);


    /**
     * 查询用户的所有权限
     */
    List<String> queryAllPerms(Long userId);
}
