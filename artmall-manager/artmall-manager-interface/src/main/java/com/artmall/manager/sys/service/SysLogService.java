package com.artmall.manager.sys.service;

import com.artmall.manager.sys.pojo.SysLogs;
import com.baomidou.mybatisplus.service.IService;

public interface SysLogService extends IService<SysLogs>{

}
