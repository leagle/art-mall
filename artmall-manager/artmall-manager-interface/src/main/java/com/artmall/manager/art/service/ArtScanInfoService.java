package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtScanInfo;
import com.baomidou.mybatisplus.service.IService;

public interface ArtScanInfoService extends IService<ArtScanInfo> {
}
