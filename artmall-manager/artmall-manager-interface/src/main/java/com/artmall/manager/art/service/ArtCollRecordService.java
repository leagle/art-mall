package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtCollRecord;
import com.artmall.manager.art.vo.ArtInfoCoRecordVO;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

public interface ArtCollRecordService extends IService<ArtCollRecord> {

    Page<ArtInfoCoRecordVO> queryArtInfoCoRecordList(Integer pageNo, Integer pageSize, String searchKey, String searchValue, Integer artTypeId, Integer accState, SysUser user);

}
