package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtPosition;
import com.baomidou.mybatisplus.service.IService;

public interface ArtPositionService extends IService<ArtPosition> {
}
