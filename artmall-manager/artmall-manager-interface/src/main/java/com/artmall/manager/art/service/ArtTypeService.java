package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtType;
import com.baomidou.mybatisplus.service.IService;

public interface ArtTypeService extends IService<ArtType> {
}
