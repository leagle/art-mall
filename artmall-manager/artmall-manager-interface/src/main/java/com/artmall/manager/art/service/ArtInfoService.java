package com.artmall.manager.art.service;

import com.artmall.manager.art.pojo.ArtInfo;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

public interface ArtInfoService extends IService<ArtInfo> {

    Page<ArtInfoVO> queryArtInfoList(Integer pageNo, Integer pageSize, String searchKey, String searchValue, Integer artTypeId, Integer status,SysUser user);
}
