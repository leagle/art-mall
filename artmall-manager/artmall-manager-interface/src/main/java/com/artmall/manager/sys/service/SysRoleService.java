package com.artmall.manager.sys.service;

import com.artmall.manager.sys.pojo.SysRole;
import com.baomidou.mybatisplus.service.IService;

public interface SysRoleService extends IService<SysRole>{
}
