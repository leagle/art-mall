package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtImage;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description:  艺术品图片
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/22 11:08
  */
public interface ArtImageMapper extends BaseMapper<ArtImage> {

}