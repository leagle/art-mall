package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysMenu;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     *  根据父菜单，查询子菜单
     */
    List<SysMenu> queryListParentId(Long parentId);


    /**
     *  根据父菜单以及菜单类型，查询子菜单
     */
    List<SysMenu> queryListParentIdAndMenuType(@Param(value = "parentId") Long parentId,@Param(value = "menuType") Integer menuType);

}