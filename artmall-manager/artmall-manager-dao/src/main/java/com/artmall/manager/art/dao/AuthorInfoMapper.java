package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.AuthorInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description: 作者信息
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/22 11:09
  */
public interface AuthorInfoMapper extends BaseMapper<AuthorInfo> {

}