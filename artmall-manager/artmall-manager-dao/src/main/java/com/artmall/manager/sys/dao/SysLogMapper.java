package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysLogs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface SysLogMapper extends BaseMapper<SysLogs> {

}