package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
  * @Description: 艺术品信息
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/22 11:09
  */
public interface ArtInfoMapper extends BaseMapper<ArtInfo> {


}