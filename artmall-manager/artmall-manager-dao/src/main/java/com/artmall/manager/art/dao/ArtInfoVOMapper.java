package com.artmall.manager.art.dao;

import com.artmall.manager.art.vo.ArtInfoVO;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description: 艺术品 VO
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/23 10:25
 */
public interface ArtInfoVOMapper extends BaseMapper<ArtInfoVO> {

    List<ArtInfoVO> queryArtInfoList(Page<ArtInfoVO> page, @Param(value = "searchKey") String searchKey,@Param(value = "searchValue") String searchValue,@Param(value = "artTypeId") Integer artTypeId,@Param(value = "status") Integer status,@Param(value = "userId") Long userId);
}
