package com.artmall.manager.art.dao;

import com.artmall.manager.art.vo.ArtInfoCoRecordVO;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArtInfoCoRecordVOMapper {

    List<ArtInfoCoRecordVO> queryArtInfoCoRecordList(Page<ArtInfoCoRecordVO> page, @Param(value = "searchKey") String searchKey, @Param(value = "searchValue") String searchValue, @Param(value = "artTypeId") Integer artTypeId, @Param(value = "accState") Integer accState, @Param(value = "userId") Long userId);
}
