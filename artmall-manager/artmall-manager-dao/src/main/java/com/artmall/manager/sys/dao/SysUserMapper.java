package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

public interface SysUserMapper extends BaseMapper<SysUser> {


    /**
     *  查询用户所有权限
     */
    List<String> queryAllPerms(Long userId);
}