package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface SysRoleMapper extends BaseMapper<SysRole> {
}