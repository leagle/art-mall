package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysRoleDept;
import tk.mybatis.mapper.common.Mapper;

public interface SysRoleDeptMapper extends Mapper<SysRoleDept> {

}