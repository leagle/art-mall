package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtPosition;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description: 艺术品初始 添加申请鉴定 位置
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/24 14:51
  */
public interface ArtPositionMapper extends BaseMapper<ArtPosition> {

}