package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtCollRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface ArtCollRecordMapper extends BaseMapper<ArtCollRecord> {

}