package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysRoleMenu;
import tk.mybatis.mapper.common.Mapper;

public interface SysRoleMenuMapper extends Mapper<SysRoleMenu> {

}