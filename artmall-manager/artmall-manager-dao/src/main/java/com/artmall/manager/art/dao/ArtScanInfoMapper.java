package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtScanInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description: 艺术品扫描信息
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/24 17:59
  */
public interface ArtScanInfoMapper extends BaseMapper<ArtScanInfo> {

}