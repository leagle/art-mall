package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.UserInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description: 用户信息
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/23 10:23
  */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}