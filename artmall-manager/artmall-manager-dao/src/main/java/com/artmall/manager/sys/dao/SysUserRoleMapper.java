package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysUserRole;
import tk.mybatis.mapper.common.Mapper;

public interface SysUserRoleMapper extends Mapper<SysUserRole> {

}