package com.artmall.manager.sys.dao;

import com.artmall.manager.sys.pojo.SysDept;
import tk.mybatis.mapper.common.Mapper;

public interface SysDeptMapper extends Mapper<SysDept> {

}