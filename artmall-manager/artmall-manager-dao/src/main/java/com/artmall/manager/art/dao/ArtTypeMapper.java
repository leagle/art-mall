package com.artmall.manager.art.dao;

import com.artmall.manager.art.pojo.ArtType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
  * @Description: 艺术品类别
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2019/1/22 11:09
  */
public interface ArtTypeMapper extends BaseMapper<ArtType> {

}