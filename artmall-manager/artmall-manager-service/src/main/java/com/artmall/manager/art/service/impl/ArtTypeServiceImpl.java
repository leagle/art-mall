package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.ArtTypeMapper;
import com.artmall.manager.art.pojo.ArtType;
import com.artmall.manager.art.service.ArtTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:18
 */
@Service
public class ArtTypeServiceImpl extends ServiceImpl<ArtTypeMapper, ArtType> implements ArtTypeService {
}
