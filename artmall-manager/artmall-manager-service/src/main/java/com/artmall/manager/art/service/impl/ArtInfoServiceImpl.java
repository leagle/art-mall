package com.artmall.manager.art.service.impl;

import com.artmall.common.utils.StringUtil;
import com.artmall.manager.art.dao.ArtInfoMapper;
import com.artmall.manager.art.dao.ArtInfoVOMapper;
import com.artmall.manager.art.pojo.ArtInfo;
import com.artmall.manager.art.service.ArtInfoService;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:17
 */
@Service
public class ArtInfoServiceImpl extends ServiceImpl<ArtInfoMapper, ArtInfo> implements ArtInfoService {

    @Autowired
    ArtInfoVOMapper artInfoVOMapper;

    @Override
    public Page<ArtInfoVO> queryArtInfoList(Integer pageNo, Integer pageSize, String searchKey, String searchValue, Integer artTypeId, Integer status,SysUser user) {

        Page<ArtInfoVO> page = new Page<>(pageNo,pageSize);

        List<ArtInfoVO> artInfoVOList = artInfoVOMapper.queryArtInfoList(page,searchKey,searchValue,artTypeId,status,user.getUserId());

        return page.setRecords(artInfoVOList);
    }

}
