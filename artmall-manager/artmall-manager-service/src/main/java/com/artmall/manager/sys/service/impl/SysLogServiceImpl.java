package com.artmall.manager.sys.service.impl;

import com.artmall.manager.sys.service.SysLogService;
import com.artmall.manager.sys.dao.SysLogMapper;
import com.artmall.manager.sys.pojo.SysLogs;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/11/29 16:57
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper,SysLogs> implements SysLogService {

}
