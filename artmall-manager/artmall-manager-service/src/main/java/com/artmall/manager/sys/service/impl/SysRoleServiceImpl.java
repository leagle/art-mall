package com.artmall.manager.sys.service.impl;

import com.artmall.manager.sys.dao.SysRoleMapper;
import com.artmall.manager.sys.pojo.SysRole;
import com.artmall.manager.sys.service.SysRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/12/11 14:13
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
