package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.ArtPositionMapper;
import com.artmall.manager.art.pojo.ArtPosition;
import com.artmall.manager.art.service.ArtPositionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/24 14:57
 */
@Service
public class ArtPositionServiceImpl extends ServiceImpl<ArtPositionMapper, ArtPosition> implements ArtPositionService {
}
