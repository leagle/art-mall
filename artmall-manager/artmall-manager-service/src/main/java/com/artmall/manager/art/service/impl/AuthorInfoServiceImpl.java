package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.AuthorInfoMapper;
import com.artmall.manager.art.pojo.AuthorInfo;
import com.artmall.manager.art.service.AuthorInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:14
 */
@Service
public class AuthorInfoServiceImpl extends ServiceImpl<AuthorInfoMapper,AuthorInfo> implements AuthorInfoService {
}
