package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.ArtScanInfoMapper;
import com.artmall.manager.art.pojo.ArtScanInfo;
import com.artmall.manager.art.service.ArtScanInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/24 17:59
 */
@Service
public class ArtScanInfoServiceImpl extends ServiceImpl<ArtScanInfoMapper, ArtScanInfo> implements ArtScanInfoService {
}
