package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.ArtImageMapper;
import com.artmall.manager.art.pojo.ArtImage;
import com.artmall.manager.art.service.ArtImageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:18
 */
@Service
public class ArtImageServiceImpl extends ServiceImpl<ArtImageMapper, ArtImage> implements ArtImageService {
}
