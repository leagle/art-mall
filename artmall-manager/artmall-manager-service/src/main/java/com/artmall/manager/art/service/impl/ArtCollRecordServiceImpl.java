package com.artmall.manager.art.service.impl;

import com.artmall.manager.art.dao.ArtCollRecordMapper;
import com.artmall.manager.art.dao.ArtInfoCoRecordVOMapper;
import com.artmall.manager.art.pojo.ArtCollRecord;
import com.artmall.manager.art.service.ArtCollRecordService;
import com.artmall.manager.art.vo.ArtInfoCoRecordVO;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/24 17:25
 */
@Service
public class ArtCollRecordServiceImpl extends ServiceImpl<ArtCollRecordMapper, ArtCollRecord> implements ArtCollRecordService {

    @Autowired
    ArtInfoCoRecordVOMapper artInfoCoRecordVOMapper;

    @Override
    public Page<ArtInfoCoRecordVO> queryArtInfoCoRecordList(Integer pageNo, Integer pageSize, String searchKey, String searchValue, Integer artTypeId, Integer accState, SysUser user) {

        Page<ArtInfoCoRecordVO> page = new Page<>(pageNo,pageSize);
        List<ArtInfoCoRecordVO> artInfoCoRecordVOList = artInfoCoRecordVOMapper.queryArtInfoCoRecordList(page,searchKey,searchValue,artTypeId,accState,user.getUserId());

        return page.setRecords(artInfoCoRecordVOList);
    }

}
