package com.artmall.manager.common.annotation;

import java.lang.annotation.*;

/**
  * @Description: 系统日志注解
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2018/11/29 16:29
  */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    String value() default "";

}