package com.artmall.manager.art.controller;

import com.artmall.common.utils.R;
import com.artmall.manager.art.service.ArtCollRecordService;
import com.artmall.manager.art.vo.ArtInfoCoRecordVO;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.sys.controller.AbstractController;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/24 18:14
 */
@RestController
@RequestMapping("/art/artCollRecord")
public class ArtCollRecordController extends AbstractController {


    @Autowired
    ArtCollRecordService artCollRecordService;

    @RequestMapping("/list")
    public R list(@RequestParam Integer page, @RequestParam Integer limit, @RequestParam(required = false) String searchKey, @RequestParam(required = false) String searchValue,@RequestParam(defaultValue = "0") Integer artTypeId,@RequestParam(defaultValue = "-2") Integer accState){

        SysUser user = getUser();

        Page<ArtInfoCoRecordVO> artInfoCoRecordVOPage = artCollRecordService.queryArtInfoCoRecordList(page, limit, searchKey, searchValue, artTypeId, accState, user);

        if(artInfoCoRecordVOPage.getTotal() != 0) {
            return R.ok().put("data", artInfoCoRecordVOPage.getRecords()).put("count", artInfoCoRecordVOPage.getTotal());
        }else{
            return R.error(200,"数据为空");
        }
    }

}
