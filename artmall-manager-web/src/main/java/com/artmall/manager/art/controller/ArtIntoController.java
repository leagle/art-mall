package com.artmall.manager.art.controller;

import com.artmall.common.utils.DateUtils;
import com.artmall.common.utils.IDUtils;
import com.artmall.common.utils.R;
import com.artmall.manager.art.pojo.*;
import com.artmall.manager.art.service.ArtCollRecordService;
import com.artmall.manager.art.service.ArtImageService;
import com.artmall.manager.art.service.ArtInfoService;
import com.artmall.manager.art.service.ArtPositionService;
import com.artmall.manager.art.vo.ArtInfoVO;
import com.artmall.manager.common.annotation.SysLog;
import com.artmall.manager.sys.controller.AbstractController;
import com.artmall.manager.sys.pojo.SysUser;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @Description: 艺术品信息
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:20
 */
@RestController
@RequestMapping("/art/artInfo")
public class ArtIntoController extends AbstractController {

    @Autowired
    ArtInfoService artInfoService;

    @Autowired
    ArtImageService artImageService;

    @Autowired
    ArtPositionService artPositionService;

    @Autowired
    ArtCollRecordService artCollRecordService;



    /**
     *  艺术品信息
     */
    @RequestMapping("/list")
    public R list(@RequestParam Integer page, @RequestParam Integer limit, @RequestParam(required = false) String searchKey, @RequestParam(required = false) String searchValue,@RequestParam(defaultValue = "0") Integer artTypeId,@RequestParam(defaultValue = "-1") Integer status){

        SysUser user = getUser();

        Page<ArtInfoVO> artInfoVOPage = artInfoService.queryArtInfoList(page, limit, searchKey, searchValue, artTypeId, status,user);
        if(artInfoVOPage.getTotal() != 0){
            return R.ok().put("data",artInfoVOPage.getRecords()).put("count",artInfoVOPage.getTotal());
        }else {
            return R.error(200,"数据为空");
        }

    }


    @SysLog("添加艺术品信息")
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public R save(ArtInfo artInfo, ArtImage artImage, ArtPosition artPosition, ArtCollRecord artCollRecord){

        // 添加艺术品信息
        String artId = IDUtils.getId();
        artInfo.setArtId(artId);
        artInfo.setAddTime(new Date());
        artInfo.setBelongUserId(getUser().getUserId().toString());
        boolean insert = artInfoService.insert(artInfo);
        // 添加艺术品图片
        artImage.setArtId(artId);
        artImage.setCollectTime(new Date());
        artImageService.insert(artImage);
        // 添加艺术品位置
        artPosition.setArtId(artId);
        artPosition.setAddTime(new Date());
        artPositionService.insert(artPosition);

        // 添加艺术品 申请采集记录
        artCollRecord.setcId(IDUtils.getcid());
        artCollRecord.setArtId(artId);
        artCollRecord.setApplyTime(new Date());
        artCollRecord.setAccState(0);
        artCollRecord.setApplyUserId(getUser().getUserId().toString());
        artCollRecordService.insert(artCollRecord);

        return R.ok();
    }
}
