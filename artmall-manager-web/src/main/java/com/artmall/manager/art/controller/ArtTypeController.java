package com.artmall.manager.art.controller;

import com.artmall.common.utils.R;
import com.artmall.manager.art.pojo.ArtType;
import com.artmall.manager.art.service.ArtTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 艺术品类型
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2019/1/22 11:23
 */

@RestController
@RequestMapping("/art/artType")
public class ArtTypeController {

    @Autowired
    ArtTypeService artTypeService;


    /**
      * @Description: 艺术品类型列表
    　*/
    @RequestMapping("/list")
    public R list(){
        List<ArtType> artTypeList = artTypeService.selectList(null);
        return R.ok().put("artTypeList",artTypeList);

    }
}
