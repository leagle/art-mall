package com.artmall.manager.art.controller;

import com.artmall.common.utils.FastDFSClient;
import com.artmall.common.utils.R;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/12/25 15:02
 */
@RestController
@RequestMapping("/pic")
public class PictureController {

    /**
     *  取 配置文件里面的属性值
     */
    @Value("${IMAGE_SERVER_URL}")
    private String IMAGE_SERVER_URL;

    /**
     *  图片上传
     */
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public R upload(MultipartFile file){

        String url;
        try {
            FastDFSClient fastDFSClient = new FastDFSClient("conf/client.conf");
            // 取文件扩展名
            String originalFilename = file.getOriginalFilename();
            String extName = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
            url = fastDFSClient.uploadFile(file.getBytes(),extName);
            url = IMAGE_SERVER_URL + url;
            System.out.println(url);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
        return R.ok().put("url",url);
    }
}
