package com.artmall.manager.sys.controller;

import com.artmall.common.utils.R;
import com.artmall.manager.common.annotation.SysLog;
import com.artmall.manager.sys.pojo.SysUser;
import com.artmall.manager.sys.service.SysUserService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/11/22 12:11
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController{

    @Autowired
    SysUserService sysUserService;

    @RequestMapping("/getUserById")
    public R getUserById(@RequestParam Long id){
        SysUser sysUser = sysUserService.getUserById(id);
        return R.ok().put("sysUser",sysUser);
    }

    /**
     *  查询用户
     */
    @RequestMapping("/list")
    public R list(@RequestParam Integer page,@RequestParam Integer limit,@RequestParam(required = false) String searchKey,@RequestParam(required = false) String searchValue){
        R r = sysUserService.getAllUser(page, limit, searchKey, searchValue);
        return r;
    }

    /**
     *  获取登录的用户信息
     */
    @RequestMapping("/info")
    public R info(){
        return R.ok().put("user",getUser());
    }

    /**
     *   添加用户
     */
    @SysLog("添加用户")
    @RequestMapping("/save")
    public R save(@RequestBody SysUser sysUser){

        sysUserService.save(sysUser);

        return R.ok();
    }


    /**
     *   修改用户
     */
    @SysLog("修改用户")
    @RequestMapping("/update")
    public R update(@RequestBody SysUser sysUser){
        sysUserService.update(sysUser);
        return R.ok();
    }

    /**
     *   删除用户
     */
    @SysLog("删除用户")
    @RequestMapping("/delete")
        public R delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds,1L)){
            return R.error("系统管理员不能删除");
        }
        if(ArrayUtils.contains(userIds, getUserId())){
            return R.error("当前用户不能删除");
        }
        sysUserService.deleteBatchIds(Arrays.asList(userIds));
        return R.ok();
    }

}
