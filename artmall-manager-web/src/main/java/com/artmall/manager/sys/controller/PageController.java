package com.artmall.manager.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
  * @Description: 页面跳转
  *
  * @author MING
  * @eamil lmm_work@163.com
  * @date 2018/11/20 10:28
  */
@Controller
public class PageController {

    @RequestMapping("/{page}")
    public String defaultPage(@PathVariable String page){
        return ""+ page;
    }

    /**
     * 控制台 跳转页面
     */
    @RequestMapping("/home/{page}")
    public String index(@PathVariable String page){
        return "home/"+ page;
    }

    /**
     *  用户 角色 菜单 跳转页面
     */
    @RequestMapping("/sys/{url}/{page}")
    public String userPage(@PathVariable String url,@PathVariable String page){
        return "sys/"+url+"/"+ page;
    }

    /**
     *  艺术品 跳转页面
     */
    @RequestMapping("/art/{url}/{page}")
    public String artPage(@PathVariable String url,@PathVariable String page){
        return "art/"+url+"/"+ page;
    }

}
