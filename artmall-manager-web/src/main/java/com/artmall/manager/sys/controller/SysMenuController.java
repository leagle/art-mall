package com.artmall.manager.sys.controller;

import com.artmall.common.utils.R;
import com.artmall.manager.common.annotation.SysLog;
import com.artmall.manager.sys.service.SysMenuService;
import com.artmall.manager.sys.pojo.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/11/22 15:42
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController{

    @Autowired
    SysMenuService sysMenuService;

    /**
     *  导航菜单
     */
    @RequestMapping("/nav")
    public R navigation(){
        List<SysMenu> menuList = sysMenuService.getUserMenuList(getUserId());
        return R.ok().put("menuList",menuList);
    }


    @RequestMapping("/nav2")
    public R navigation(Integer menuType){
        List<SysMenu> menuList = sysMenuService.getUserMenuList(getUserId(),menuType);
        return R.ok().put("menuList",menuList);
    }


    /**
     *  所有菜单列表
     */
    @RequestMapping("/list")
    public R list(){
        List<SysMenu> menuList = sysMenuService.selectList(null);
        for (SysMenu sysMenu : menuList){
            SysMenu parentMenu = sysMenuService.selectById(sysMenu.getParentId());
            if (parentMenu !=null){
                sysMenu.setParentName(parentMenu.getName());
            }
        }
        return R.ok().put("data",menuList);
    }


    @RequestMapping("queryListParentId")
    public R queryListParentId(){
        List<SysMenu> menuList = sysMenuService.queryListParentId(0L);
        return  R.ok().put("menuList",menuList);
    }

    /**
     * 添加菜单
     */
    @SysLog("添加菜单")
    @RequestMapping("/save")
    public R save(@RequestBody SysMenu sysMenu){
        sysMenuService.insert(sysMenu);
        return R.ok();
    }

    /**
     * 添加菜单
     */
    @SysLog("删除菜单")
    @RequestMapping("/delete")
    public R delete(Long menuId) {

        if (menuId <= 31) {
            return R.error("系统菜单，不能删除");
        }
        //判断是否有子菜单或按钮
        List<SysMenu> menuList = sysMenuService.queryListParentId(menuId);
        if (menuList.size() > 0) {
            return R.error("请先删除子菜单或按钮");
        }
        sysMenuService.delete(menuId);

        return R.ok();
    }
}
