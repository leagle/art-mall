package com.artmall.manager.sys.controller;

import com.artmall.common.utils.R;
/*import com.google.code.kaptcha.Constants;*/
import com.artmall.manager.common.utils.ShiroUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: MING
 * @Eamil lmm_work@163.com
 * @Date: 2018/12/6 15:00
 */
@RestController
@RequestMapping("/sys")
public class LoginController {


    /**
     *  登录
     * @param username
     * @param password
     * @param captcha
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public R login(String username,String password,String captcha){

/*
        String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
        if(!captcha.equalsIgnoreCase(kaptcha)){
            return R.error("验证码不正确");
        }
*/

        try {
            Subject subject = ShiroUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(username,password);
            subject.login(token);
        }catch (UnknownAccountException e) {
            return R.error(e.getMessage());
        }catch (IncorrectCredentialsException e) {
            return R.error("账号或密码不正确");
        }catch (LockedAccountException e) {
            return R.error("账号已被锁定,请联系管理员");
        }catch (AuthenticationException e) {
            return R.error("账户验证失败");
        }

        return R.ok();
    }

    /**
     *  退出
     * @return
     */
    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public R logout(){
        ShiroUtils.logout();
        return R.ok();
    }
}
