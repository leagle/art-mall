/**
 * @Description: 艺术品采集记录
 *
 * @Author MING
 * @Email  lmm_work@163.com
 * @Date   2019/1/22 15:13
 　*/

layui.define(['table', 'form','upload','laytpl'], function(exports){
    var $ = layui.$,
        table = layui.table,
        form = layui.form,
        admin = layui.admin,
        configs = layui.configs,
        upload = layui.upload,
        laytpl= layui.laytpl;

    //艺术品信息显示
    table.render({
        elem: '#lay-tab'
        ,url: configs.base_server +'/art/artCollRecord/list'
        ,toolbar: true
        ,cols: [[
            {type: 'checkbox'}
            ,{field: 'artCollRecord', width: 150, title: '采集编号', templet: '<div>{{d.artCollRecord.cId}}</div>'}
            ,{field: 'artInfo', width: 150, title: '艺术品数字身份证', templet: '<div>{{d.artInfo.icdn}}</div>'}
            ,{field: 'artInfo', title: '艺术品名称', minWidth: 80, width: 100,templet: '<div>{{d.artInfo.artName}}</div>'}
            ,{field: 'artType', title: '类别', minWidth: 60, width: 100,templet: '<div>{{d.artType.artTypeName}}</div>'}
            ,{field: 'artCollRecord',title: '当前状态',templet: '#art-status', minWidth: 80, width: 100}
            ,{field: 'artCollRecord',title: '申请时间',sort: true,templet: '<div>{{d.artCollRecord.applyTime}}</div>'}
            ,{title: '操作', width: 180, align:'center', fixed: 'right', toolbar: '#table-operation'}
        ]]
        ,page: true
        ,limit: 10
        ,height: 'full-80'
        ,text: '对不起，加载出现异常！'
    });


    /* 艺术品类别 */

    $.ajax({
        url : configs.base_server + "/art/artType/list",
        type:'get',
        data:{},
        success:function (data) {

            var $html = "";
            if(data.artTypeList != null){
                $.each(data.artTypeList, function (index, item) {
                    $html += "<option value='" + item.artTypeId + "'>" + item.artTypeName + "</option>";
                });
                $("select[name='artTypeId']").append($html);

                //append后必须从新渲染
                form.render('select');
            }
        }
    })


    //监听搜索
    form.on('submit(lay-submit-search)', function(data){
        var field = data.field;

        //执行重载
        table.reload('lay-tab', {
            where: field
        });
    });


    //监听工具条
    table.on('tool(lay-tab)', function(obj){
        var data = obj.data;
        if(obj.event === 'details'){

            layer.open({
                type: 2
                ,title: '查看详情'
                ,content: 'artcollrecordform.html'
                ,maxmin: true
                ,area: ['700px', '450px']
                ,success: function(layero, index){

                    var div = layero.find('iframe').contents().find('#lay-form');

                    var artcolldata = obj.data;
                    div.find('#cId').val(artcolldata.artCollRecord.cId);
                    div.find('#applyTime').val(artcolldata.artCollRecord.applyTime);
                    div.find('#icdn').val(artcolldata.artInfo.icdn !=null? artcolldata.artInfo.icdn : "暂无数据");
                    div.find('#artName').val(artcolldata.artInfo.artName);
                    div.find('#artTypeName').val(artcolldata.artType.artTypeName);
                    div.find('#size').val(artcolldata.artInfo.artNormsLong+";"+artcolldata.artInfo.artNormsWide+";"+artcolldata.artInfo.artNormsHeight);
                    if(artcolldata.artScanInfo != null){
                        div.find('#hyperspectral').val(artcolldata.artScanInfo.hyperspectral !=null? artcolldata.artScanInfo.hyperspectral : "暂无数据");
                        div.find('#highPrecision').val(artcolldata.artScanInfo.highPrecision !=null? artcolldata.artScanInfo.highPrecision : "暂无数据");
                        div.find('#trueColor').val(artcolldata.artScanInfo.trueColor !=null? artcolldata.artScanInfo.trueColor : "暂无数据");
                    }else {
                        div.find('#hyperspectral').val("暂无数据");
                        div.find('#highPrecision').val("暂无数据");
                        div.find('#trueColor').val("暂无数据");
                    }

                    // 进度条
                    div.find("#step").val(artcolldata.artCollRecord.accState);


                }
            });

        }
    });


    var imgListView = $("#imgList"),
        uploadListIns =  upload.render({
            elem: '#uploadImg'
            ,url: configs.base_server + 'pic/upload/'
            ,auto: false
            ,multiple: true
            ,bindAction: '#uploadBtn'
            // ,before: function(obj){
            ,choose: function(obj){
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td width="100px">'+ file.name +'</td>'
                        ,'<td><img src="'+ result +'" alt="'+ file.name +'" style="width: 150px;height: 150px;"></td>'
                        ,'<td>'+ (file.size/1014).toFixed(1) +'kb</td>'
                        ,'<td>等待上传</td>'
                        ,'<td>'
                        ,'<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    imgListView.append(tr);
                    $('.imageTab').removeClass('layui-hide');
                });
            }
            ,done: function(res, index, upload){

                if(res.code == 0){ //上传成功
                    // 拼接图片的 url
                    var img_url =  $("#image_url").val();
                    img_url += res.url+";";
                    $("#image_url").val(img_url);
                    $("#cover_image_url").val(res.url);

                    var tr = imgListView.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(4).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            ,error: function(index, upload){
                var tr = imgListView.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(3).html('<span style="color: #FF5722;">上传失败</span>');
                tds.eq(4).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
        });

    exports('artcollrecordlist', {})
});