 **  项目说明** 

- 采用SpringMvc、Spring、MyBatis、Shiro框架，开发的一套权限管理系统。使用Layui前端框架完成后台管理页面。

 
 **    项目结构：** 

    art-mall                                 父工程、管理jar包的版本号、打包方式 pom。项目中所有工程都应该继承父工程。
        |--artmall-common                    通用的工具类、打包方式jar
        |--artmall-manager                   服务层工程、聚合工程、打包方式 pom
            |--artmall-manager-dao                      打包方式 jar           
            |--artmall-manager-interface                打包方式 jar
            |--artmall-manager-pojo                     打包方式 jar
            |--artmall-manager-service                  打包方式 war
        |--artmall-manager-web               表现层工程、打包方式 war

   **  项目架构 ：** 
    
    1、 SOA 架构（Service-Oriented Architecture 面向服务的架构）
        【SOA 一种粗粒度、松耦合服务架构，服务之间通过简单、精确定义接口进行通讯，不涉及底层编程接口和通讯模型】
    2、 分布式框架  Dubbo + Zookeeper  +  SSM 
        【Dubbo 使用rpc协议进行远程调用，直接使用socket通信。】
        【Zookeeper 负责服务地址的注册与查找，是一个树型的目录服务，支持变更推送。工业强度较高，可用于生产环境。】
        【SSM SpringMVC/Spring/MyBatis/MybatisPlus】
    3、 LayUI 框架
        【LayUI 一款采用自身模块规范编写的前端 UI 框架，遵循原生 HTML/CSS/JS 的书写与组织形式。】
    4、 MYSQL 数据库
    
    权限管理 - 用户 -角色 -部门
    系统设置 - 菜单 -参数 -字典 定时任务 -文件上传
    日志管理 - 登录日志 -操作日志 -异常日志
    系统检测 -SQL监控
    
    
   
    
  **注：**
  
    -- 安装 分页 pagehelper-3.4.2-fix.jar 到本地仓库,这个是修改过的
    mvn install:install-file -Dfile=C:\Users\MING\Desktop\pagehelper-3.4.2-fix.jar -DgroupId=com.github.pagehelper -DartifactId=pagehelper -Dversion=3.4.2-fix -Dpackaging=jar -DgeneratePom=true

